#ifndef NATIVE_H
#define NATIVE_H

#define FOREACH_NATIVE(OP)\
		OP(CLASS_NEW, nf_new)\
		OP(INT_TO_STR, nf_int_to_str)\
		OP(INT_ADD, nf_int_add)\
		OP(INT_MUL, nf_int_mul)\
		OP(INT_DIV, nf_int_div)\
		OP(INT_SUB, nf_int_sub)\
		OP(INT_LT, nf_int_lt)\
		OP(INT_GT, nf_int_gt)\
		OP(INT_NE, nf_int_ne)\
		OP(INT_EQ, nf_int_eq)\
		OP(INT_LE, nf_int_le)\
		OP(INT_GE, nf_int_ge)\
		OP(INT_MIN, nf_int_min)\
		OP(INT_TO_HEX_STR, nf_int_to_hex_str)\
		OP(STR_LENGTH, nf_str_length)\
		OP(STR_TO_INT, nf_str_to_int)\
		OP(STR_CONCAT, nf_concat)\
		OP(STR_CMP, nf_str_cmp)\
		OP(STR_AT, nf_str_at)\
		OP(VOID_TYPE, nf_void_type)\
		OP(ADDR_TO_INT, nf_addr_to_int)\
		OP(ARRAY_NEW, nf_array_new)\
		OP(ARRAY_INDEX, nf_array_index)\
		OP(ARRAY_WRITE, nf_array_write)\
		OP(ARRAY_LENGTH, nf_array_length)\
		OP(SYS_BT, nf_sys_bt)\
		OP(SYS_OUT, nf_sys_out)\
		OP(SYS_IN, nf_sys_in)\
		OP(SYS_EXIT, nf_sys_exit)\
		OP(ASSERT_STRING, nf_assert_string)\
		OP(ASSERT_INT, nf_assert_int)\
		OP(IS_NIL, nf_is_nil)\

#define GENERATE_NATIVE_ENUM(ENUM, FUNC) ENUM,
#define GENERATE_NATIVE_STRING(STRING, FUNC) #STRING,
#define GENERATE_NATIVE_FNC(STRING, FUNC) &FUNC,

enum NFunc {
	FOREACH_NATIVE(GENERATE_NATIVE_ENUM)
};

extern char *native_str[];

int native_index(char *native_name);
#endif
