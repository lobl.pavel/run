#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hash.h"

#define LOAD_FACTOR		0.75

/* TODO: rehashing not implemented yet */
/* source: http://planetmath.org/goodhashtableprimes */
static unsigned int primes[] = { 5, 13, 23, 53, 97, 193, 389, 1543, 3079, 6151, 12289, \
					 24593, 49157, 98317, 196613, 393241, 786433, 1572869, \
					 3145739, 6291469, 12582917, 25165843, 50331653, \
					 100663319, 201326611, 402653189, 805306457, 1610612741 };


static unsigned int htable_size(HTable *tab) {
	return primes[tab->size_index];
}

void htable_iterator_init(HTable *tab, HTIter *iter) {
	iter->index = 0;
	iter->p = NULL;
	htable_iterator_next(tab, iter);
}

void htable_iterator_next(HTable *tab, HTIter *iter) {
	if (iter->p == NULL) {
		int i;
		for (i = iter->index; i < htable_size(tab); i++) {
			if (tab->table[i]) {
				iter->p = tab->table[i];
				iter->index = i;
				break;
			}
		}
	} else {
		iter->p = iter->p->next;
		if (iter->p == NULL) {
			iter->index++;
			htable_iterator_next(tab, iter);
		}
	}
}

Entry *htable_iterator_entry(HTIter *iter) {
	return iter->p;
}

void *htable_iterator_obj(HTIter *iter) {
	if (iter->p)
		return iter->p->data;
	else
		return NULL;
}

/*
static unsigned int htable_full(HTable *tab) {
	return htable_size(tab)*LOAD_FACTOR < tab->entries;
}

static void htable_rehash(HTable *tab) {
	unsigned int new_size = primes[tab->size_index + 1];
	Entry **new_array = (Entry**)tab->alloc(new_size*sizeof(Entry*));
}
*/

unsigned long htable_djb2(const char *str) {
	unsigned long hash = 5381;
	int c;
	while ((c = *str++))
		hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
	return hash;
}

void htable_init(HTable *tab, void* (*allocator)(size_t), void delete(void*)) {
	int size;
	tab->delete = delete;
	tab->alloc = allocator;
	tab->size_index = 0;
	tab->entries = 0;
	size = htable_size(tab);
	tab->table = (struct Entry**)tab->alloc(sizeof(Entry*)*size);
	memset(tab->table, 0, size * sizeof(Entry*));
	return;
}

void *htable_find(HTable *tab, const char *key) {
	unsigned long hash = htable_djb2(key);
	unsigned index = hash % htable_size(tab);

	Entry *p_entry = tab->table[index];
	for (; p_entry != NULL; p_entry = p_entry->next) {
		if (strcmp(p_entry->key, key) == 0)
			return p_entry->data;
	}
	return NULL;
}

void htable_remove(HTable *tab, const char *key) {
	unsigned long hash = htable_djb2(key);
	unsigned index = hash % htable_size(tab);

	Entry *p_entry = tab->table[index];
	Entry **p_last_next = &tab->table[index];
	while (p_entry)
		if (strcmp(p_entry->key, key) == 0)
			break;
		else {
			p_last_next = &p_entry->next;
			p_entry = p_entry->next;
	}
	/* if key not found */
	if (!p_entry)
		return;

	*p_last_next = p_entry->next;
	tab->delete(p_entry->data);
	free((char*)p_entry->key);
	free(p_entry);
	tab->entries--;
}

void htable_free(HTable *tab) {
	Entry *p_entry, *tmp;
	unsigned int i;
	for (i = 0; i < htable_size(tab); ++i) {
		p_entry = tab->table[i];
		while (p_entry != NULL) {
			tmp = p_entry;
			p_entry = p_entry->next;
			tab->delete(tmp->data);
			free((char*)tmp->key);
			free(tmp);
		}
	}
	free(tab->table);
}

void htable_foreach(HTable *tab, void (callback)(const char*, void*)) {
	Entry *p_entry;
	unsigned int i;
	for (i = 0; i < htable_size(tab); ++i) {
		p_entry = tab->table[i];
		for (; p_entry != NULL; p_entry = p_entry->next) {
			callback(p_entry->key, p_entry->data);
		}
	}
}


static Entry **entry_prev_next(Entry **table, const char *key, unsigned int index) {
	Entry *pe = table[index];
	Entry **last = &table[index];
	for (; pe; pe = pe->next) {
	//	printf("pe: %p %s %s\n", pe, key, pe->key);
		if (strcmp(pe->key, key) == 0) {
			return NULL;
		} else {
		}
		last = &pe->next;
	}
	return last;
}

int htable_add(HTable *tab, const char *key, void *data) {
	unsigned long hash = htable_djb2(key);
	unsigned int index = hash % htable_size(tab);

	Entry *new;
	Entry **last = entry_prev_next(tab->table, key, index);
	if (last == NULL) {
		return 0;
	}

	new = (Entry*)tab->alloc(sizeof(Entry));
	if (new == NULL) {
		perror("alloc");
		exit(EXIT_FAILURE);
	}
	new->data = data;
	new->next = NULL;
	new->key = (char*)tab->alloc(sizeof(char)*strlen(key) + 1);
	strcpy((char*)new->key, key);

	*last = new;
	tab->entries++;
/*
	if (htable_full(tab)) {
	}
*/
	return 1 == 1;
}
