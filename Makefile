CC=gcc
CFLAGS=-Wall -I./jsmn/
LDFLAGS=-lfl -lm -lgmp -L./jsmn/ -ljsmn
BISONFLAGS=
BIN_DIR=bin

all: sub-jsmn compile vm

debug: CFLAGS += -g -D_DEBUG
debug: all
#debug: BISONFLAGS += --debug --report-state -rall

vm: hash.o vm.o cons.o ident.o stack_arg.o alloc.o native.o class_parse.o class.o
	$(CC) vm.o hash.o alloc.o class_parse.o code.o native.o class.o cons.o stack_arg.o ident.o -o vm $(LDFLAGS)
	mv vm $(BIN_DIR)

class.o: class.h class.h
	$(CC) $(CFLAGS) -c class.c

native.o: native.c native.h
	$(CC) $(CFLAGS) -c native.c

stack_arg.o: stack_arg.c stack_arg.h
	$(CC) $(CFLAGS) -c stack_arg.c

alloc.o: alloc.c alloc.h
	$(CC) $(CFLAGS) -c alloc.c

ast.o: ast.c ast.h
	$(CC) $(CFLAGS) -c ast.c

class_parse.o: class_parse.c class_parse.h
	$(CC) $(CFLAGS) -c class_parse.c

vm.o: vm.c native.h
	$(CC) $(CFLAGS) -c vm.c

ident.o: ident.h ident.c
	$(CC) $(CFLAGS) -c ident.c

cons.o: cons.c cons.h
	$(CC) $(CFLAGS) -c cons.c

compile: parser.o code.o cons.o ident.o native.o hash.o
	$(CC) $(CFLAGS)  hash.o parser.o ast.o lexer.o native.o ident.o code.o cons.o -o compile $(LDFLAGS)
	mv compile $(BIN_DIR)

hash.o: hash.c hash.h
	$(CC) $(CFLAGS) -c hash.c

parser.o: parser.c lexer.c
	$(CC) $(CFLAGS) -c lexer.c
	$(CC) $(CFLAGS) -c parser.c

code.o: code.h code.c
	$(CC) $(CFLAGS) -c code.c

lexer.c: parser.lex
	flex -o lexer.c --header-file=lexer.h parser.lex

parser.c: ast.o parser.y
	bison $(BISONFLAGS) -o parser.c -d parser.y

clean:
	$(MAKE) -C ./jsmn/ clean
	rm -f *o parser.c lexer.c parser.h lexer.h parser.output vm compile
	rm -f $(BIN_DIR)/vm $(BIN_DIR)/compile
	rm -f native/*class examples/*class

sub-jsmn:
	$(MAKE) -C ./jsmn/
