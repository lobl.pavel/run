#define _GNU_SOURCE
#include <stdio.h>
#include "code.h"
#include <stdlib.h>
#include "hash.h"
#include "ident.h"
#include "cons.h"
#include "ast.h"
#include "debug.h"

#define cmp_error(...) fprintf(stderr, "error: ");\
						fprintf(stderr, __VA_ARGS__);\
						 exit(EXIT_FAILURE);

static char *int_to_string(int d) {
  char buffer[512];
  snprintf(buffer, 512, "%d", d);
  return strndup(buffer, 512);
}

void break_print(struct AstNode *node, void *data) {
    struct tables *tb_data = (struct tables*)data;
    struct Inst i = { .opcode = JMP, .op1 = NULL, .op2 = NULL };
    code_pushi(tb_data->code, i);
}

void continue_print(struct AstNode *node, void *data) {
    struct tables *tb_data = (struct tables*)data;
    struct Inst i = { .opcode = JMP,
					  .op1 = int_to_string(tb_data->before_loop - code_get_pc(tb_data->code)), .op2 = NULL };
    code_pushi(tb_data->code, i);
}

struct AstNode* continue_create() {
	struct AstNode *new_cont = (struct AstNode*)malloc(sizeof(struct AstNode));
	new_cont->print = &continue_print;
	return new_cont;
}

struct AstNode* break_create() {
	struct AstNode *new_break = (struct AstNode*)malloc(sizeof(struct AstNode));
	new_break->print = &break_print;
	return new_break;
}

void assign_print(struct AstNode *assign, void *data) {
    struct tables *tb_data = (struct tables*)data;

	char *ident = assign->assign.ident;
	int index;
    struct Inst i;
	i.op2 = NULL;

	assign->assign.expr->print(assign->assign.expr, data);
	if ((ident_table_contains(tb_data->parsed_fields, ident))) {
		i.opcode = SET_IVAR;
		i.op1 = int_to_string(ident_table_index(tb_data->parsed_fields, ident));
	} else {
		index = ident_table_index(tb_data->local_vars, ident);
		i.opcode = SET_LOCAL;
		i.op1 = int_to_string(index);
	}
	code_pushi(tb_data->code, i);
}

void return_print(struct AstNode* ret, void *data) {
    struct tables *tb_data = (struct tables*)data;
    struct Inst i = { .opcode = RET, .op1 = NULL, .op2 = NULL };
	ret->ret.expr->print(ret->ret.expr, data);
    code_pushi(tb_data->code, i);
    return;
}

struct AstNode* return_create(struct AstNode *expr) {
	struct AstNode *new_return = (struct AstNode*)malloc(sizeof(struct AstNode));
	new_return->ret.expr = expr;
	new_return->print = &return_print;
	return new_return;
}


void native_print(struct AstNode* call, void *data) {
    struct tables *tb_data = (struct tables*)data;
    if (call->native.args)
        call->native.args->print(call->native.args, data);
    struct Inst i = { .opcode = CALL_NATIVE, .op1 = call->native.value, .op2 = NULL };
    code_pushi(tb_data->code, i);
    return;
}

struct AstNode* native_create(char *ident, struct AstNode *call_args) {
	struct AstNode *new_native = (struct AstNode*)malloc(sizeof(struct AstNode));
	new_native->native.value = ident;
	new_native->print = &native_print;
	new_native->native.args = call_args;
    return new_native;
}

struct AstNode*	assign_create(char *ident, struct AstNode *expr) {
	struct AstNode *new_assign = (struct AstNode*)malloc(sizeof(struct AstNode));
	new_assign->assign.ident = ident;
	new_assign->assign.expr = expr;
	new_assign->print = &assign_print;
	return new_assign;
}

struct AstNode* if_create(struct AstNode *cond,
                          struct AstNode *if_branch, struct AstNode *else_branch) {
    struct AstNode *new_if = (struct AstNode*)malloc(sizeof(struct AstNode));
    new_if->print = &if_print;
    new_if->cond.if_branch = if_branch;
    new_if->cond.else_branch = else_branch;
    new_if->cond.expr = cond;
    return new_if;
}

void loop_print(struct AstNode* loop, void *data) {
    struct tables *tb_data = (struct tables*)data;
	int jmp_before = code_get_pc(tb_data->code);
    loop->loop.expr->print(loop->loop.expr, data);

	int before_backup = tb_data->before_loop;
	tb_data->before_loop = jmp_before;
    int jmp_cond = code_push_nop(tb_data->code);
    if (loop->loop.body)
        loop->loop.body->print(loop->loop.body, data);

	tb_data->before_loop = before_backup;

    struct Inst i = { .opcode = JCMP,
                      .op1 = int_to_string(code_get_pc(tb_data->code) - jmp_cond + 1),
                      .op2 = NULL };
    code_insert_at(tb_data->code, i, jmp_cond);

	struct Inst jmp = { .opcode = JMP,
   					    .op1 = int_to_string(-code_get_pc(tb_data->code) + jmp_before),
                        .op2 = NULL };
	code_pushi(tb_data->code, jmp);
	int jmp_after = code_get_pc(tb_data->code);
	/* patch the break loop jumps */
	int j;
	for (j = jmp_before; j < jmp_after; j++) {
		struct Inst *ins = &tb_data->code->c[j];
		if (ins->opcode == JMP && ins->op1 == NULL)
			ins->op1 = int_to_string(jmp_after - j);
	}
}


struct AstNode* loop_create(struct AstNode *cond, struct AstNode *body) {
    struct AstNode *new_loop = (struct AstNode*)malloc(sizeof(struct AstNode));
    new_loop->print = &loop_print;
    new_loop->loop.body = body;
    new_loop->loop.expr = cond;
    return new_loop;
}

void classref_print(struct AstNode* n, void *data) {
    struct tables *tb_data = (struct tables*)data;
	struct literal class_ref = { .type = C_CLASS, strdup(n->string.value) };
	int index = cons_table_add(tb_data->class_cons, class_ref);
    struct Inst i = { .opcode = PUSH_CONST, .op1 = int_to_string(index), .op2 = NULL };
    code_pushi(tb_data->code, i);
}

void nil_print(struct AstNode* n, void *data) {
    struct tables *tb_data = (struct tables*)data;
	struct literal string_ref = { .type = C_NIL, strdup("") };
	int index = cons_table_add(tb_data->class_cons, string_ref);
    struct Inst i = { .opcode = PUSH_CONST, .op1 = int_to_string(index), .op2 = NULL };
    code_pushi(tb_data->code, i);
}

struct AstNode* nil_create() {
    struct AstNode *new_nil = (struct AstNode*)malloc(sizeof(struct AstNode));
    new_nil->print = &nil_print;
    return new_nil;
}

void string_print(struct AstNode* n, void *data) {
    struct tables *tb_data = (struct tables*)data;
	struct literal string_ref = { .type = C_STRING, strdup(n->string.value) };
	int index = cons_table_add(tb_data->class_cons, string_ref);
    struct Inst i = { .opcode = PUSH_CONST, .op1 = int_to_string(index), .op2 = NULL };
    code_pushi(tb_data->code, i);
}

struct AstNode* classref_create(char *value) {
    struct AstNode *new_ref = (struct AstNode*)malloc(sizeof(struct AstNode));
    new_ref->string.value = value;
    new_ref->print = &classref_print;
    return new_ref;
}

struct AstNode* string_create(char *value) {
    struct AstNode *new_string = (struct AstNode*)malloc(sizeof(struct AstNode));
    new_string->string.value = value;
    new_string->print = &string_print;
    return new_string;
}

void int_print(struct AstNode* n, void *data) {
    struct tables *tb_data = (struct tables*)data;
	struct literal int_ref = { .type = C_INT, strdup(n->integer.value) };
	int index = cons_table_add(tb_data->class_cons, int_ref);
    struct Inst i = { .opcode = PUSH_CONST, .op1 = int_to_string(index), .op2 = NULL };
    code_pushi(tb_data->code, i);
}

struct AstNode* int_create(char *value) {
    struct AstNode *new_int = (struct AstNode*)malloc(sizeof(struct AstNode));
    new_int->integer.value = value;
    new_int->print = &int_print;
    return new_int;
}

void var_print(struct AstNode* n, void *data) {
    struct tables *tb_data = (struct tables*)data;
	char *ident = n->var.name;
    struct Inst i;
	i.op1 = NULL;
	i.op2 = NULL;
	if (strcmp(ident, "self") == 0) {
		i.opcode = PUSH_SELF;
	} else {
		DEBUG_PRINT("VAR PRINT %s\n", ident);
#ifdef _DEBUG
		if (debug)
			ident_table_print(tb_data->parsed_fields);
#endif
		if ((ident_table_contains(tb_data->parsed_fields, ident))) {
			i.opcode = PUSH_IVAR;
			i.op1 = int_to_string(ident_table_index(tb_data->parsed_fields, ident));
		} else {
			i.opcode = PUSH_LOCAL;
			i.op1 = int_to_string(ident_table_index(tb_data->local_vars, n->var.name));
		}
	}
    code_pushi(tb_data->code, i);
}

struct AstNode* var_create(char *name) {
    struct AstNode *new_var = (struct AstNode*)malloc(sizeof(struct AstNode));
    new_var->var.name = name;
    new_var->print = &var_print;
    return new_var;
}

void un_min_print(struct AstNode *op, void *data) {
    op->un_min.expr->print(op->un_min.expr, data);
}

struct AstNode* un_min_create(struct AstNode *expr) {
    struct AstNode *new_min = (struct AstNode*)malloc(sizeof(struct AstNode));
    new_min->un_min.expr = expr;
    new_min->print = &un_min_print;
    return new_min;
}

void call_arg_print(struct AstNode *arg, void *data) {
    arg->call_arg.expr->print(arg->call_arg.expr, data);
    if (arg->call_arg.next) {
        arg->call_arg.next->print(arg->call_arg.next, data);
    }
    return;
}


struct AstNode* call_arg_create(struct AstNode *expr) {
    struct AstNode *new_arg = (struct AstNode*)malloc(sizeof(struct AstNode));
    new_arg->call_arg.expr = expr;
    new_arg->call_arg.next = NULL;
    new_arg->print = &call_arg_print;
    return new_arg;
}

void call_arg_set_next(struct AstNode *arg, struct AstNode *next) {
    arg->call_arg.next = next;
}

struct AstNode* stm_create(struct AstNode *expr) {
    struct AstNode *new_stm = (struct AstNode*)malloc(sizeof(struct AstNode));
    new_stm->stm.stm = expr;
    new_stm->stm.next = NULL;
    new_stm->print = &stm_print;
    DEBUG_PRINT("stm created: [%p] stm: [%p]\n", (void*)new_stm, (void*)expr);
    return new_stm;
}

void stm_set_next(struct AstNode *stm, struct AstNode *next) {
    stm->stm.next = next;
}

void line_print(struct AstNode *line, void *data) {
	struct tables *tb_data = (struct tables*)data;
	struct Inst i = { .opcode = POP, .op1 = NULL, .op2 = NULL };
	if (line->stm.stm) {
        line->stm.stm->print(line->stm.stm, data);
	}
	code_pushi(tb_data->code, i);
}

struct AstNode* line_create(struct AstNode *expr) {
    struct AstNode *new_stm = (struct AstNode*)malloc(sizeof(struct AstNode));
    new_stm->stm.stm = expr;
    new_stm->stm.next = NULL;
    new_stm->print = &line_print;
    return new_stm;
}

void stm_print(struct AstNode *stm, void *data) {
    stm->stm.stm->print(stm->stm.stm, data);
    if (stm->stm.next) {
        stm->stm.next->print(stm->stm.next, data);
	}
}

struct AstNode* funcall_create(struct AstNode* object, char *method, struct AstNode *call_args) {
    struct AstNode *new_call = (struct AstNode*)malloc(sizeof(struct AstNode));
    new_call->call.object = object;
    new_call->call.method = method;
    new_call->call.call_args = call_args;
	new_call->call.super = 0;
    new_call->print = &funcall_print;
    DEBUG_PRINT("funcall created: [%p] object: [%p] %s\n", new_call, object, method);
    return new_call;
}

void if_print(struct AstNode* cond, void *data) {
    cond->cond.expr->print(cond->cond.expr, data);

    struct tables *tb_data = (struct tables*)data;
    int jmp_cond = code_push_nop(tb_data->code);
    int jmp_end = code_get_pc(tb_data->code);
    if (cond->cond.if_branch) {
        cond->cond.if_branch->print(cond->cond.if_branch, data);
        if (cond->cond.else_branch)
			jmp_end = code_push_nop(tb_data->code);
    }

    struct Inst i = { .opcode = JCMP,
                      .op1 = int_to_string(code_get_pc(tb_data->code) - jmp_cond),
                      .op2 = NULL };
    code_insert_at(tb_data->code, i, jmp_cond);

    if (cond->cond.else_branch) {
        cond->cond.else_branch->print(cond->cond.else_branch, data);
        struct Inst jmp = { .opcode = JMP,
                            .op1 = int_to_string(code_get_pc(tb_data->code) - jmp_end),
                            .op2 = NULL };
        code_insert_at(tb_data->code, jmp, jmp_end);
    }
}

int call_arg_num(struct AstNode *call_args) {
	int n = 0;
	struct AstNode *p = call_args;
	while (p) {
		p = p->call_arg.next;
		n++;
	}
	return n;
}

void funcall_print(struct AstNode *call, void *data) {
    struct tables *tb_data = (struct tables*)data;
    if (call->call.call_args) {
        call->call.call_args->print(call->call.call_args, data);
    }
    call->call.object->print(call->call.object, data);

	int n  = call_arg_num(call->call.call_args);
	char *name = NULL;
	if (strcmp("main", call->call.method) == 0) //||
//		strcmp("init", call->call.method) == 0)
		name = strdup(call->call.method);
	else
		asprintf(&name, "%s_%d", call->call.method, n);

	struct literal method_ref = { .type = C_METHOD, strdup(name) };
	free(name);
	int index = cons_table_add(tb_data->class_cons, method_ref);
    struct Inst i = { .opcode = CALL, .op1 = int_to_string(index), .op2 = int_to_string(n) };
	if (call->call.super)
		i.opcode = CALL_SUPER;
    code_pushi(tb_data->code, i);
    return;
}

struct AstNode* field_create(char *name, struct AstNode *next) {
    struct AstNode *new_field = (struct AstNode*)malloc(sizeof(struct AstNode));
    new_field->field.name = name;
    new_field->field.next = next;
    new_field->print = &field_print;
    return new_field;
}

int field_list_contains(struct AstNode *f, char *var) {
	int i = 0;
	while(f) {
		if (strcmp(var, f->field.name) == 0)
			return i;
		f = f->field.next;
		i++;
	}
	return -1;
}

void field_set_next(struct AstNode *f, struct AstNode* next) {
    f->field.next = next;
}

void field_print(struct AstNode *f, void *data) {
    struct tables *tb_data = (struct tables*)data;
	ident_table_index(tb_data->parsed_fields, f->field.name);
    if (f->field.next)
        f->field.next->print(f->field.next, data);
}

void proc_set_next(struct AstNode* p, struct AstNode* next) {
    p->proc.next = next;
}

void proc_print(struct AstNode *th, void *data) {
    struct tables *tb_data = (struct tables*)data;

	tb_data->local_vars = &th->proc.local_vars;
	tb_data->code = &th->proc.code;


	struct AstNode *p;
	int number_of_args = 0;
	for (p = th->proc.args; p; p = p->field.next) {
		number_of_args++;
		if (ident_table_contains(tb_data->parsed_fields, p->field.name)) {
			cmp_error("in method %s.%s local `%s` shadows instance variable\n",
			tb_data->class_current->class.this_name, th->proc.name, p->field.name);
		}
		ident_table_index(tb_data->local_vars, p->field.name);
	}
	char *name = NULL;
	asprintf(&name, "%s_%d", th->proc.name, number_of_args);

	if (htable_find(&tb_data->procs, name) != NULL) {
		cmp_error("method %s.%s redefined\n", tb_data->class_current->class.this_name, name);
	}
	htable_add(&tb_data->procs, name, NULL);
	free(name);

	if (strcmp("main", th->proc.name) == 0)// ||
//		strcmp("init", th->proc.name) == 0)
	    fprintf(tb_data->class_out, "{ \"name\" : \"%s\", ", th->proc.name);
	else
	    fprintf(tb_data->class_out, "{ \"name\" : \"%s_%d\", ", th->proc.name, number_of_args);

    fprintf(tb_data->class_out, "\"args\" : %d", number_of_args);
    if (th->proc.stms) {
        th->proc.stms->print(th->proc.stms, tb_data);
    }
	CODE *c = &th->proc.code;
	// TODO: what if used_size is 0?
	if (c->used_size == 0 || c->c[c->used_size - 1].opcode != RET) {
		struct Inst i1 = { .opcode = PUSH_SELF, .op1 = NULL, .op2 = NULL};
		struct Inst i2 = { .opcode = RET, .op1 = NULL, .op2 = NULL};
		code_pushi(c, i1);
		code_pushi(c, i2);
	}
	fprintf(tb_data->class_out, ", \"locals\" : %d", tb_data->local_vars->size);
    fprintf(tb_data->class_out, ", \"code\" : [");
    //fprintf(class_out, "] ");
	char buffer[256];
	int i;

	for (i = 0; i < tb_data->code->used_size - 1; i++) {
		inst_sprintf(&tb_data->code->c[i], buffer);
		fprintf(tb_data->class_out, "\"%s\", ", buffer);
	}
	if (tb_data->code->used_size) {
		inst_sprintf(&tb_data->code->c[i], buffer);
		fprintf(tb_data->class_out, "\"%s\"", buffer);
	}

	ident_table_free(tb_data->local_vars);

    fprintf(tb_data->class_out, " ] }");

	code_free(c);

    if (th->proc.next) {
        fprintf(tb_data->class_out, ", ");
        th->proc.next->print(th->proc.next, data);
    }
}

struct AstNode* proc_create(char *name, struct AstNode *stms,
                            struct AstNode *args, struct AstNode *next) {
    struct AstNode *new_proc = (struct AstNode*)malloc(sizeof(struct AstNode));
    new_proc->proc.name = name;
    new_proc->proc.args = args;
    new_proc->proc.next = next;
    new_proc->proc.stms = stms;
    new_proc->print = &proc_print;
	code_init(&new_proc->proc.code);
	ident_table_init(&new_proc->proc.local_vars);
    return new_proc;
}

void class_set_next(struct AstNode *this, struct AstNode* next) {
    this->class.next = next;
}

struct AstNode* class_create(char *this_name, char* super_name,
                             struct AstNode *fields, struct AstNode *procs,
                             struct AstNode *next) {
    struct AstNode *new_class = (struct AstNode*)malloc(sizeof(struct AstNode));
    new_class->class.this_name = this_name;
    new_class->class.super_name = super_name;
    new_class->class.procs = procs;
    new_class->class.fields = fields;
    new_class->class.next = next;
    new_class->print = &class_print;
	cons_table_init(&new_class->class.class_cons);
    DEBUG_PRINT("class created: [%p]\n", (void*)new_class);
    return new_class;
}

void class_print(struct AstNode *c, void *data) {

    char *super = c->class.super_name;
    if (super == NULL) {
        super = "Void";
    }
	DEBUG_PRINT("PARSING CLASS %s SUPER %s\n", c->class.this_name, super);
	struct ident_table *ivars = htable_find((HTable*)data, super);
	if (ivars == NULL) {
		ivars = ident_table_alloc();
	} else {
#ifdef _DEBUG
		DEBUG_PRINT("IVAR TABLE %s FOUND\n", super);
		if (debug)
			ident_table_print(ivars);
#endif
		ivars = ident_table_copy(ivars);
	}
	htable_add((HTable*)data, c->class.this_name, ivars);

	struct tables tb_data = { .class_cons = &c->class.class_cons,
							  .code = NULL, .local_vars = NULL,
							  .parsed_fields = ivars,
							  .class_current = c };

	htable_init(&tb_data.procs, &malloc, &free);

    char *file_name;
	asprintf(&file_name, "%s.class", c->class.this_name);
    tb_data.class_out = fopen(file_name, "w+");

    if (tb_data.class_out == NULL) {
        fprintf(stderr, "%s\n", file_name);
        perror("fopen");
        exit(2);
    }

    fprintf(tb_data.class_out, "{ \"name\" : \"%s\"", c->class.this_name);
    fprintf(tb_data.class_out, ", \"super\" : \"%s\", ", super);

    if (c->class.fields != NULL)
        c->class.fields->print(c->class.fields, &tb_data);

	/* print procs */
    fprintf(tb_data.class_out, "\"procs\" : [ ");
    if (c->class.procs != NULL)
    	c->class.procs->print(c->class.procs, &tb_data);
    fprintf(tb_data.class_out, "], ");
	fprintf(tb_data.class_out, "\"const\" : [ ");

	int i;
	struct cons_table *class_cons = tb_data.class_cons;
	for (i = 0; i < class_cons->size - 1; i++) {
		fprintf(tb_data.class_out, "[\"%s\", \"%s\"], ",
				cons_type_str[class_cons->table[i].type],
				class_cons->table[i].name);
	}
	if (class_cons->size)
		fprintf(tb_data.class_out, "[\"%s\", \"%s\"]",
				cons_type_str[class_cons->table[i].type],
				class_cons->table[i].name);

	DEBUG_PRINT("IVAR TABLE\n");
#ifdef _DEBUG
	if (debug)
		ident_table_print(ivars);
#endif
    fprintf(tb_data.class_out, "], \"fields\" : [");
	for (i = 0; i < ivars->size - 1; i++)
    	fprintf(tb_data.class_out, "\"%s\", ", ivars->name[i]);
	if (ivars->size > 0)
    	fprintf(tb_data.class_out, "\"%s\"", ivars->name[i]);


    fprintf(tb_data.class_out, "]");
    fprintf(tb_data.class_out, "}\n");

	cons_table_free(class_cons);
    fclose(tb_data.class_out);

	printf("%s\n", file_name);
	free(file_name);

	htable_free(&tb_data.procs);

    if (c->class.next != NULL)
        c->class.next->print(c->class.next, data);
}
