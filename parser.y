%{
#include <stdio.h>
#include "ast.h"
#include "cons.h"
#include "lexer.h"
#include "debug.h"

extern int yylineno;
extern char *yytext;
extern FILE *yyin;

static struct AstNode *root;

//static struct cons_table *cons;

int yyerror(const char *s);
%}

%union
{
    char    *sval;
    int     *ival;
    struct AstNode *node;
};

%locations
%error-verbose

/* Generate the parser description file.  */
%verbose
     /* Enable run-time traces (yydebug).  */

     /* Formatting semantic values.
%printer { fprintf (yyoutput, "%s", $$->name); } VAR;
%printer { fprintf (yyoutput, "%s()", $$->name); } FNCT;
%printer { fprintf (yyoutput, "%g", $$); } <double>;
*/
%type <node> Fields
%type <node> Procedure
%type <node> Procedures
%type <node> Statements
%type <node> Statement
%type <node> Class
%type <node> Clases
%type <node> While
%type <node> Expr
%type <node> If
%type <node> ArgList
%type <node> Args
%type <node> CallArgs
%type <node> CallArgsList
%type <node> ArrayList


%token <sval> STRING	"sting literal"
%token <sval> IDENTIFIER "indentifier"
%token <sval> MIDENTIFIER "method identifier"
%token <sval> NATIVE
%token <sval> CLASS_IDENTIFIER	"class indentifier"
%token <sval> INTEGER	"integer literal"
%token UN_MIN "unary minus"
%token BIN_PLUS "+"
%token BIN_MIN  "-"
%token BIN_MUL  "*"
%token BIN_DIV  "/"
%token BIN_AND	"&&"
%token BUN_OR	"||"
%token DOT  "."
%token COMMA  ","

%nonassoc IDENTIFIER
%nonassoc MIDENTIFIER
%nonassoc INTEGER


%right BIN_ASSIGN
%left BIN_AND BIN_OR
%left BIN_GT BIN_LT BIN_EQ BIN_NE BIN_LE BIN_GE
%left BIN_PLUS BIN_MIN
%left BIN_DIV BIN_MUL
%left FCALL
%nonassoc LBR
%nonassoc LPAR
%nonassoc UNMIN
%left DOT

%token PROCEDURE "proc"
%token KW_IF "if"
%token KW_THEN "then"
%token KW_ELSE "else"
%token KW_END "end"
%token KW_CLASS "class"
%token KW_SUPER "super"
%token KW_RETURN "return"
%token KW_BREAK "break"
%token KW_CONTINUE "continue"

%token CR "new line"
%token SEMI
%token NIL "nil"

%token RPAR ")"
%token LPAR "("
%token RBR  "]"
%token LBR  "["

%token KW_WHILE

%start Start
%%

Start: Clases { root = $1; }
	| CR Start
	;

Clases: Class CSep Clases { $$ = $1; class_set_next($1, $3); }
    | { $$ = NULL; }
    ;

CSep: CR
	| CSep CR;

Class: KW_CLASS CLASS_IDENTIFIER CR Fields Procedures KW_END
      { $$ = class_create($2, NULL, $4, $5, NULL); }
    | KW_CLASS CLASS_IDENTIFIER KW_SUPER CLASS_IDENTIFIER CR Fields Procedures KW_END
      { $$ = class_create($2, $4, $6, $7, NULL); }
    ;

Fields: IDENTIFIER CR Fields
    { $$ = field_create($1, $3); }
    | { $$ = NULL; }
    ;

Procedures: Procedure PSep Procedures { $$ = $1; proc_set_next($1, $3); }
    | { $$ = NULL; }
    ;

PSep: CR
	| PSep CR;

Statements: Statement StaSep Statements {
		$$ = stm_create($1);
		stm_set_next($$, $3);
	}
    | { $$ = NULL;  }
    ;

StaSep : CR
	   | StaSep CR;



Procedure: PROCEDURE IDENTIFIER CR Statements KW_END
    { $$ = proc_create($2, $4, NULL, NULL); }
    | PROCEDURE IDENTIFIER LPAR Args RPAR CR Statements KW_END
    { $$ = proc_create($2, $7, $4, NULL); } /* ident, args, code, next */
    ;

Args: ArgList { $$ = $1; }
    | { $$ = NULL; }

ArgList: IDENTIFIER COMMA ArgList
    { $$ = field_create($1, $3); }
	| IDENTIFIER { $$ = field_create($1, NULL); };
    ;

Expr:  Expr BIN_PLUS Expr {$$ = funcall_create($1, "add", call_arg_create($3)); }
     | Expr BIN_GT Expr { $$ = funcall_create($1, "gt", call_arg_create($3)); }
     | Expr BIN_GE Expr { $$ = funcall_create($1, "ge", call_arg_create($3)); }
     | Expr BIN_LT Expr { $$ = funcall_create($1, "lt", call_arg_create($3)); }
     | Expr BIN_LE Expr { $$ = funcall_create($1, "le", call_arg_create($3)); }
     | Expr BIN_EQ Expr { $$ = funcall_create($1, "eq", call_arg_create($3)); }
     | Expr BIN_NE Expr { $$ = funcall_create($1, "ne", call_arg_create($3)); }
     | Expr BIN_MIN Expr { $$ = funcall_create($1, "sub", call_arg_create($3)); }
     | Expr BIN_MUL Expr { $$ = funcall_create($1, "mul", call_arg_create($3)); }
     | Expr BIN_DIV Expr { $$ = funcall_create($1, "div", call_arg_create($3)); }
     | Expr BIN_AND Expr { $$ = funcall_create($1, "and", call_arg_create($3)); }
     | Expr BIN_OR Expr { $$ = funcall_create($1, "or", call_arg_create($3)); }
     | BIN_MIN Expr %prec UNMIN
       { $$ = funcall_create($2, "min", NULL); }
     | LPAR Expr RPAR
       { $$ = $2; }
     | KW_SUPER DOT IDENTIFIER %prec FCALL
		 { struct AstNode *fnc = funcall_create(var_create("self"), $3, NULL);
		   fnc->call.super = 1; $$ = fnc; }
     | KW_SUPER DOT IDENTIFIER LPAR CallArgs RPAR %prec FCALL
		 { struct AstNode *fnc = funcall_create(var_create("self"), $3, $5);
		   fnc->call.super = 1; $$ = fnc;  }
     | Expr DOT IDENTIFIER %prec FCALL {
		if (strcmp($3, "new") == 0) {
			$$ = funcall_create(funcall_create($1, $3, NULL), "init", NULL);
		} else
			$$ = funcall_create($1, $3, NULL); }
     | Expr DOT IDENTIFIER LPAR CallArgs RPAR %prec FCALL {
		if (strcmp($3, "new") == 0)
			$$ = funcall_create(funcall_create($1, $3, NULL), "init", $5);
		else
			$$ = funcall_create($1, $3, $5); }
     | Expr DOT IDENTIFIER BIN_ASSIGN Expr %prec FCALL {
			$$ = funcall_create($1, $3, call_arg_create($5)); }

     | NATIVE LPAR CallArgs RPAR %prec FCALL
       { $$ = native_create($1, $3); }
     | IDENTIFIER BIN_ASSIGN Expr
       { $$ = assign_create($1, $3); }
	 | Expr LBR Expr RBR BIN_ASSIGN Expr {
			struct AstNode *cargs = call_arg_create($3);
			call_arg_set_next(cargs, call_arg_create($6));
			$$ = funcall_create($1, "write", cargs);
		}
	 | Expr LBR Expr RBR { $$ = funcall_create($1, "at", call_arg_create($3)); }
     | IDENTIFIER
       { $$ = var_create($1); }
     | CLASS_IDENTIFIER
       { $$ = classref_create($1); }
     | INTEGER
       { $$ = int_create($1); }
     | STRING
       { $$ = string_create($1); }
	 | LBR ArrayList RBR { $$ = $2; }
/*	 | NIL { $$ = funcall_create(classref_create("Nil"), "new", NULL); } */
     | NIL { $$ = nil_create(); }
	 | LBR RBR {	$$ = funcall_create(
									funcall_create(
										classref_create("Array"),
										"new",
										NULL
									),
								"init",
								call_arg_create(int_create("0"))); };

ArrayList: ArrayList COMMA Expr { $$ = funcall_create($1, "push", call_arg_create($3)); }
		 	 | Expr { $$ = funcall_create(  /* this is C, not lisp */
						   		funcall_create(
									funcall_create(
										classref_create("Array"),
										"new",
										NULL
									),
								"init",
								call_arg_create(int_create("0"))),
							"push",
							call_arg_create($1)); }


CallArgs:  CallArgsList { $$ = $1; }
         | { $$ = NULL; } /* no call arguments */
         ;

CallArgsList: Expr COMMA CallArgsList
             { $$ = call_arg_create($1); call_arg_set_next($$, $3); }
             | Expr
             { $$ = call_arg_create($1); }
             ;

While: KW_WHILE Expr CR Statements KW_END { $$ = loop_create($2, $4); }
    ;

If: KW_IF Expr CR Statements KW_END
    { $$ = if_create($2, $4, NULL); }
    | KW_IF Expr CR Statements KW_ELSE CR Statements KW_END
    { $$ = if_create($2, $4, $7);}
    ;


Statement: Expr { $$ = line_create($1); }
/*	| NATIVE { $$ = native_create($1); }*/
    | If
    | While
	| KW_RETURN Expr { $$ = return_create($2);  }
	| KW_BREAK { $$ = break_create(); }
	| KW_CONTINUE { $$ = continue_create(); }
    ;

%%
int
yyerror(const char *s) {
    fprintf(stderr, "%d:%d: %s at %s\n", yylineno, yylloc.first_column, s, yytext);
    exit(EXIT_FAILURE);
}


void value_delete(void *data) {
    free(data);
}

#ifdef _DEBUG
	int debug;
#endif

void free_table(void *table) {
	ident_table_free(table);
	free(table);
}

int main(int argc, char **argv) {

	DEBUG_INIT();

	if (argc > 2) {
		fprintf(stderr, "usage: compile [FILE]\n");
		exit(EXIT_FAILURE);
	}

	yyin = stdin;
	if (argc > 1) {
		yyin = fopen(argv[1], "r");
		if (yyin == NULL) {
			perror("fopen");
			exit(EXIT_FAILURE);
		}
	}

	HTable parsed_fields;
	htable_init(&parsed_fields, &malloc, &free_table);

	int ret = yyparse();
	if (root)
	    root->print(root, &parsed_fields);
	else
		fprintf(stderr, "nothing to parse\n");

	htable_free(&parsed_fields);

    return ret;
}
