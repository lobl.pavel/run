#include "code.h"
#include "assert.h"
#include "native.h"

const char *inst_str[] = {
	FOREACH_INST(GENERATE_STRING)
};

int *string_to_int(const char *str) {
	int *val = (int*)malloc(sizeof(int));
	*val = atoi(str);
	return val;
}

int *int_malloc(int val) {
	int *new = (int*)malloc(sizeof(int));
	*new = val;
	return new;
}

struct Code *code_alloc() {
	struct Code *c = (struct Code*)malloc(sizeof(struct Code));
	code_init(c);
	return c;
}

void code_init(struct Code *c) {
	c->allocated_size = 10;
	c->used_size = 0;
	c->c = (struct Inst*)malloc(sizeof(struct Inst)*c->allocated_size);
}

int code_push_nop(struct Code *c) {
	struct Inst i = { .opcode = NOP, .op1 = NULL, .op2 = NULL };
	return code_pushi(c, i);
}

void code_add_ret(struct Code *c) {
	if (c->used_size == 0 || c->c[c->used_size-1].opcode != RET) {
		struct Inst i = { .opcode = RET,
			.op1 = NULL,
			.op2 = NULL };
		code_pushi(c, i);
	}
	return;
}

int code_pushi(struct Code *c, struct Inst i) {
	code_insert_at(c, i, c->used_size);
	return c->used_size++;;
}

int code_insert_at(struct Code *c, struct Inst i, int pc) {
	if (c->allocated_size == c->used_size) {
		c->allocated_size *= 2;
		c->c = (struct Inst*)realloc(c->c, sizeof(struct Inst)*c->allocated_size);
	}
	c->c[pc] = i;
	return c->used_size;
}

int code_get_pc(struct Code *c) {
	return c->used_size;
}

void code_free(struct Code *c) {
	int i;
	for (i = 0; i < c->used_size; i++) {
		if (c->c[i].op2)
			free(c->c[i].op2);
		if (c->c[i].op1)
			free(c->c[i].op1);
	}
	free(c->c);
}

struct Inst inst_parse(const char* str) {
	char *tok = strdup(str);
	INST i;
	tok = strtok(tok, " ");
	assert(tok != NULL);

	unsigned j, flag = 0;
	for (j = 0; j < sizeof(inst_str)/sizeof(inst_str[0]); j++) {
		if (strcmp(inst_str[j], tok) == 0) {
			i.opcode = j;
			flag = 1;
		}
	}
	i.op1 = strtok(NULL, " ");
	if (i.op1) {
		if (i.opcode == CALL_NATIVE) {
			char *function = i.op1;
			i.op1 = int_malloc(native_index(i.op1));
			if (*(int*)i.op1 < 0) {
				fprintf(stderr, "unknown native function: %s\n", function);
				exit(EXIT_FAILURE);
			}
		} else {
			i.op1 = string_to_int(i.op1);
		}
	}
	i.op2 = strtok(NULL, " ");
	if (i.op2)
		i.op2 = string_to_int(i.op2);
	if (flag != 1)
		printf("parse instr: %s\n", str);
	assert(flag == 1);
	free(tok);
	return i;
}

/* this is for debugging */

void code_print(struct Code *c) {
	int i;
	for (i = 0; i < c->used_size; ++i) {
		printf("%3d: %s", i, inst_str[c->c[i].opcode]);
		if (c->c[i].op1)
			printf(" %d", *(int*)c->c[i].op1);
		printf("\n");
	}
}

void inst_sprintf(struct Inst *i, char *buffer) {
	char *op1;
	if (i->op1)
		op1 = i->op1;
	else
		op1 = "";
	if (i->op2)
		sprintf(buffer, "%s %s %s", inst_str[i->opcode], op1, (char*)i->op2);
	else
		sprintf(buffer, "%s %s", inst_str[i->opcode], op1);
}

void inst_sprintf_int(struct Inst *i, char *buffer) {
	if (i->op1 && i->op2)
		sprintf(buffer, "%s %d %d", inst_str[i->opcode], *(int*)i->op1, *(int*)i->op2);
	else if (i->op1)
		sprintf(buffer, "%s %d", inst_str[i->opcode], *(int*)i->op1);
	else
		sprintf(buffer, "%s", inst_str[i->opcode]);
}
