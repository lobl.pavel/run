#ifndef STACK_ARG_H
#define STACK_ARG_H
#define STACK_INITIAL_SIZE			4096
#define STACK_GROWTH_FACTOR			2

struct stack_arg {
	void **data;
	int top;
	int allocated;
};

void *stack_arg_get(struct stack_arg *stack, int index);
void stack_arg_set(struct stack_arg *stack, int index, void *val);
void stack_arg_alloc(struct stack_arg *stack, int size);
void **stack_arg_top_addr(struct stack_arg *stack);
void *stack_arg_top(struct stack_arg *stack);
void stack_arg_print(struct stack_arg *stack);
void stack_arg_free(struct stack_arg *stack);
void stack_arg_init(struct stack_arg *stack);
void *stack_arg_pop(struct stack_arg *stack);
void stack_arg_push(struct stack_arg *stack, void *arg);
#endif
