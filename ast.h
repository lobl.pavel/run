#ifndef AST_H
#define AST_H

#include "code.h"
#include "cons.h"
#include "ident.h"
#include "hash.h"

/* this structure is passed around the tree to support
 * semantic checks, code generation and other stuff */
struct tables {
	struct cons_table *class_cons;
	struct ident_table *local_vars;
	struct ident_table *parsed_fields;
	struct AstNode *class_current;
	struct Code *code;
	HTable procs;
	FILE *class_out;
	int before_loop;
};

struct AstNode {
    union {
        struct {
            char *this_name;
            char *super_name;
            struct AstNode *procs;
            struct AstNode *next;
            struct AstNode *fields; /* instance vars */
			struct cons_table class_cons; /* class constants */
        } class;
        struct {
            char *name;
            struct AstNode *stms;
            struct AstNode *args;
            struct AstNode *next;
			struct Code code;
			struct ident_table local_vars;
        } proc;
        struct {
            struct AstNode *expr;
            struct AstNode *if_branch;
            struct AstNode *else_branch;
        } cond;
		struct {
			struct AstNode *expr;
			struct AstNode *body;
		} loop;
        struct {
            struct AstNode *expr;
            struct AstNode *next;
        } expr;
        struct {
            char *name;
            struct AstNode *next;
        } field;
        struct {
            struct AstNode *stm;
            struct AstNode *next;
        } stm;
        struct {
            struct AstNode *expr;
        } un_min;
        struct {
            struct AstNode *object;
            struct AstNode *call_args;
            char *method;
			int super;
        } call;
        struct {
            struct AstNode *expr;
            struct AstNode *next;
        } call_arg;
		struct {
			char *ident;
			struct AstNode *expr;
		} assign;
		struct {
			struct AstNode *expr;
		} ret;
        struct {
            char *name;
        } var;
        struct {
            char *value;
        } string;
		struct {
			char *value;
			struct AstNode *args;
		} native;
	    struct {
            char *value;
        } classref;
        struct {
            char *value;
        } integer;
		struct {
			struct AstNode *arg_list;
		} array;
    };
    void (*print)(struct AstNode *, void*);
};

struct AstNode* nil_create();
struct AstNode* continue_create();
struct AstNode* break_create();

struct AstNode* array_create(struct AstNode *array);
struct AstNode* return_create(struct AstNode *expr);
struct AstNode* line_create(struct AstNode *expr);
struct AstNode* native_create(char *ident, struct AstNode *args);
struct AstNode* class_create(char *, char*, struct AstNode*, struct AstNode*, struct AstNode*);
void class_set_next(struct AstNode *, struct AstNode*);
void class_print(struct AstNode *c, void*);

struct AstNode* proc_create(char *, struct AstNode*, struct AstNode*, struct AstNode*);
void proc_print(struct AstNode*, void*);
void proc_set_next(struct AstNode*, struct AstNode*);

struct AstNode* field_create(char *, struct AstNode*);
void field_print(struct AstNode*, void*);
void field_set_next(struct AstNode*, struct AstNode*);
int field_list_contains(struct AstNode*, char*);

struct AstNode* loop_create(struct AstNode *cond, struct AstNode *body);
struct AstNode* if_create(struct AstNode *cond, struct AstNode *if_branch, struct AstNode *else_branch);
void if_print(struct AstNode *cond, void*);

struct AstNode*	assign_create(char *ident, struct AstNode *expr);
struct AstNode* un_min_create(struct AstNode *expr);

struct AstNode* stm_create(struct AstNode *expr);
void stm_print(struct AstNode *stm, void*);
void stm_set_next(struct AstNode *stm, struct AstNode *next);

struct AstNode* var_create(char *name);
struct AstNode* funcall_create(struct AstNode *object, char *method, struct AstNode *args);
void funcall_print(struct AstNode *call, void*);
void var_print(struct AstNode* n, void*);

struct AstNode*	call_arg_create(struct AstNode *expr);
struct AstNode*	classref_create(char *value);
struct AstNode*	int_create(char *value);
struct AstNode* string_create(char *value);
void call_arg_set_next(struct AstNode *arg, struct AstNode *next);
#endif
