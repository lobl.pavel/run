#!/bin/bash

CMP=compile

for i in ./*.lp; do
	$CMP < "$i"
	ret=$?
	if [ $ret -ne 0 ];then
		echo "cannot compile $i"
		exit $ret
	fi
done
