#include <stdio.h>
#include "class_parse.h"
#include "jsmn.h"
#include "debug.h"

struct tokens {
	jsmntok_t *tok;
	jsmntok_t *pos;
	char *buffer;
	int count;
};

typedef struct tokens TOKENS;

#define TOKEN_STRING(js, t, s) \
	(strncmp(js+(t).start, s, (t).end - (t).start) == 0 \
	 && strlen(s) == (t).end - (t).start)

#define TOKEN_PRINT(t) \
	printf("start: %d, end: %d, type: %d, size: %d\n", \
			(t).start, (t).end, (t).type, (t).size)

#define TOKEN_ECHO(buffer, tok) \
	{ char *str = strndup(buffer + (tok).start, (tok).end - (tok).start); \
	printf("token: %s\n", str); \
	free(str); }

static void parse_error(const char *str) {
	fprintf(stderr, "%s\n", str);
	exit(EXIT_FAILURE);
}


static void tokens_init(TOKENS *t, int number, int file_bytes) {
	t->buffer = (char*)calloc(file_bytes + 1, sizeof(char));
	t->tok = (jsmntok_t*)malloc(sizeof(jsmntok_t)*number);
	t->count = number;
	t->pos = t->tok;
}

static void tokens_free(TOKENS *t) {
	free(t->buffer);
	free(t->tok);
}

static char *tokens_strdup(const char *js, jsmntok_t *t) {
	return strndup(js + t->start, t->end - t->start);
}

static int parse_const(TOKENS *t, struct cons_table *table) {
	int i, size;
	t->pos++;
	if (t->pos->type != JSMN_ARRAY)
		parse_error("parse_const: bad json");
	size = t->pos->size;

	t->pos++;
	for (i = 0; i < size; i++, t->pos++) {
		t->pos++;
		char *type = tokens_strdup(t->buffer, t->pos);
		t->pos++;
		char *value = tokens_strdup(t->buffer, t->pos);
		struct literal new_const = literal_parse(type, value);
		free(type);
		free(value);
		cons_table_add(table, new_const);
	}
	return 0;
}

static int parse_proc_code(TOKENS *t, PROC *proc) {
	int i, size;
	t->pos++;
	if (t->pos->type != JSMN_ARRAY)
		parse_error("parse_proc_code: bad json");
	size = t->pos->size;
	t->pos++;

	code_init(&proc->code);
	for (i = 0; i < size; i++, t->pos++) {
		char *inst;
		INST i = inst_parse(inst = tokens_strdup(t->buffer, t->pos));
		free(inst);
		code_pushi(&proc->code, i);
	}
	return 0;
}

static int parse_proc(TOKENS *t, CLASS *class) {
	int i, size;
	if (t->pos->type != JSMN_OBJECT)
		parse_error("parse_proc: bad json");
	size = t->pos->size;
	t->pos++;
	PROC *new_proc = (PROC*)malloc(sizeof(PROC));
	char *dup;
	for (i = 0; i < size; i+=2) {
		if (TOKEN_STRING(t->buffer, *t->pos, "name")) {
			t->pos++;
			new_proc->name = tokens_strdup(t->buffer, t->pos);
			t->pos++;
		} else if (TOKEN_STRING(t->buffer, *t->pos, "locals")) {
			t->pos++;
			new_proc->locals = atoi(dup = tokens_strdup(t->buffer, t->pos));
			free(dup);
			t->pos++;
		} else if (TOKEN_STRING(t->buffer, *t->pos, "args")) {
			t->pos++;
			new_proc->num_args = atoi(dup = tokens_strdup(t->buffer, t->pos));
			free(dup);
			t->pos++;
		} else if (TOKEN_STRING(t->buffer, *t->pos, "code")) {
			parse_proc_code(t, new_proc);
		}
	}
	class_add_proc(class, new_proc);
	return 0;
}

static int parse_procs(TOKENS *t, CLASS *class) {
	int i = 0;
	int size;
	t->pos++;
	size = t->pos->size;
	if (t->pos->type != JSMN_ARRAY)
		parse_error("parse_procs: bad json");
	t->pos++;
	for (i = 0; i < size; i++) {
		parse_proc(t, class);
	}
	return 0;
}

static int parse_fields(TOKENS *t, CLASS *class) {
	int size;
	t->pos++;
	size = t->pos->size;
	if (t->pos->type != JSMN_ARRAY)
		parse_error("parse_fields: bad json");
	t->pos++;
	for (;size > 0;size--, t->pos++) {
		char *dup;
		ident_table_index(&class->fds, dup = tokens_strdup(t->buffer, t->pos));
		free(dup);
	}
	return 0;
}

//CLASS* parse_class(TOKENS *t, VM *vm, OBJECT* (*new)(VM*)) {
static CLASS *parse_class(TOKENS *t) {
	int size = t->pos->size;
	int i;
	if (t->pos->type != JSMN_OBJECT)
		parse_error("parse_class: bad json");
	CLASS *new_class = (CLASS*)malloc(sizeof(CLASS));
	class_init(new_class);
	t->pos++;
	for (i = 0; i < size; i=i+2) {
		if (TOKEN_STRING(t->buffer, *t->pos, "name")) {
			t->pos++;
			new_class->this_name = tokens_strdup(t->buffer, t->pos);
			t->pos++;
		} else if (TOKEN_STRING(t->buffer, *t->pos, "super")) {
			t->pos++;
			new_class->super_name = tokens_strdup(t->buffer, t->pos);
			t->pos++;
		} else if (TOKEN_STRING(t->buffer, *t->pos, "fields")) {
			parse_fields(t, new_class);
		} else if (TOKEN_STRING(t->buffer, *t->pos, "procs")) {
			parse_procs(t, new_class);
		} else if (TOKEN_STRING(t->buffer, *t->pos, "const")) {
			parse_const(t, &new_class->cons);
		} else {
			printf("parse_class: unknown tag");
			exit(EXIT_FAILURE);
		}
	};
	return new_class;
}


static TOKENS *parse_load_tokens(FILE *file) {
	long	numbytes;

	if(file == NULL) {
		perror("fopen");
		exit(EXIT_FAILURE);
	}

	fseek(file, 0L, SEEK_END);
	numbytes = ftell(file);
	fseek(file, 0L, SEEK_SET);

	TOKENS *tokens = (TOKENS*)malloc(sizeof(TOKENS));
	tokens_init(tokens, 1000, numbytes);

	fread(tokens->buffer, sizeof(char), numbytes, file);
	fclose(file);

	jsmn_parser p;
	jsmn_init(&p);
	int ret = jsmn_parse(&p, tokens->buffer, tokens->tok, 1000);
	if (ret != JSMN_SUCCESS) {
		fprintf(stderr, "cannot parse JSON, invalid or incomplete class\n");
		exit(EXIT_FAILURE);
	}
	return tokens;
}

CLASS *parse_load_class(FILE *file) {
	TOKENS *tks = parse_load_tokens(file);
	CLASS *ret = parse_class(tks);
	tokens_free(tks);
	free(tks);
	return ret;
}
