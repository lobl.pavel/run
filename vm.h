#ifndef VM_H
#define VM_H

#define FLAG_MARK		1
#define FLAG_NIL		2
#define FLAG_ARRAY		4
#define FLAG_INT		8
#define FLAG_STRING		16
#define FLAG_OBJECT		32

int object_marked(void *addr);
void object_mark(void *addr);
void object_unmark(void *addr);
void object_freed(void *addr);

#endif
