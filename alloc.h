#ifndef ALLOC_H
#define ALLOC_H

struct blk {
	void *addr;
	struct blk *next;
};

struct alloc {
	void *base;
	struct blk **free_list;
	struct blk **used_list;
	int *list_size;
	int list_count;
};

void alloc_collect(struct alloc *al);
void alloc_unmark(struct alloc *al);
void alloc_print_lists(struct alloc *al);
void *alloc_allocate(struct alloc *al, int size);
void alloc_init(struct alloc *al);
void alloc_free(struct alloc *al);
#endif

