#include "class.h"
#include "debug.h"

PROC *class_lookup_proc(CLASS **target, const char *name) {
	PROC *method = NULL;
	CLASS *c = *target;
	DEBUG_PRINT("search: %s in %s\n", name, c->this_name);
	while (method == NULL) {
		method = (PROC*)htable_find(&c->procs, name);
		if (c->super == c || method != NULL)
			break;
		c = c->super;
	}
	if (method == NULL) {
		*target = NULL;
		return NULL;
	} else {
		*target = c;
		return method;
	}
}

void class_init(CLASS *c) {
	htable_init(&c->procs, &malloc, &proc_free);
	cons_table_init(&c->cons);
	ident_table_init(&c->fds);
	return;
}

void class_free(void *data) {
	CLASS *c = (CLASS*)data;
	cons_table_free(&c->cons);
	free(c->this_name);
	free(c->super_name);
	htable_free(&c->procs);
	ident_table_free(&c->fds);
	free(c);
}

void proc_free(void *data) {
	struct proc *p = (struct proc*)data;
	free(p->name);
	code_free(&p->code);
	free(p);
}

void proc_print(PROC *proc) {
	DEBUG_PRINT("proc: \'%s\' args: %d\n", proc->name, proc->num_args);
	code_print(&proc->code);
}

void class_procs_print_callback(const char *key, void *data) {
	key = NULL;
	proc_print((PROC*)data);
}

void class_procs_print(CLASS *c) {
	htable_foreach(&c->procs, &class_procs_print_callback);
}

void class_add_proc(CLASS *c, PROC *p) {
	htable_add(&c->procs, p->name, p);
}

void class_fields_print(CLASS *c) {
}

void class_print(CLASS *c) {
	DEBUG_PRINT("CLASS\n");
	DEBUG_PRINT("name: %s\n", c->this_name);
	DEBUG_PRINT("super: %s\n", c->super_name);
	DEBUG_PRINT("fields:\n");
	ident_table_print(&c->fds);
	class_procs_print(c);
	DEBUG_PRINT("constant table:\n");
	cons_table_print(&c->cons);
}

struct Inst *proc_get_first_inst(PROC *p) {
	return &p->code.c[0];
}

int class_num_fields(CLASS *c) {
	return c->fds.size;
}
